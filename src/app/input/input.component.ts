import { Component, OnInit } from '@angular/core';
import { InputService } from './input.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  public value:string;
  constructor(private inputService:InputService) { }

  ngOnInit() {
  }
  public createItem():void{
    this.inputService.pushTodo(this.value);
  }

}
